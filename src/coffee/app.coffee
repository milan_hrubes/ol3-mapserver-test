#OpenLayers = require 'OpenLayers3'
VectorLayer = require './vectorLayer'

###*
   * Objekt pro vytvareni mapy
###
map =
  ###*
     * Vykreslena mapa ol3
     * @type {Object}
  ###
  renderedMap: null

  ###*
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
  ###
  vectorLayer: null

  ###*
     * Url pro zavolani mapserver
     * @type {String}
  ###
  mapserverUrl: 'http://localhost/cgi-bin/mapserv'

  ###*
     * Cesta k mapfile
     * @type {String}
  ###
  mapfileUrl: '/srv/www/htdocs/ol3-mapserver-test/src/data/mapfile.map'

  ###
    * Inicializace OL3 mapy
    * @param {String} target - id elementu pro mapu
  ###
  init: (target) ->
    @renderedMap = new ol.Map({
      controls: @setControls(),
      layers: [],
      view: @setMapView()
      target: target
    })

    @setLayers()
    @createLayerSwitcher()

    #return @renderedMap

  setMapView: ->
    return new ol.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857')
      #center: [15, 50],
      zoom: 5
      #projection: 'EPSG:4326'
      projection: 'EPSG:3857'
    })

  setLayers: ->
    # mapserver transform on fly from EPSG:4326 to EPSG: 3857
    statesLayer = new ol.layer.Image({
      title: 'Countries'
      visible: false
      source: new ol.source.ImageWMS({
        url: @mapserverUrl
        crossOrigin: 'anonymous'
        params: {
          LAYERS: 'world-political-line'
          #CRS: 'EPSG:4326',
          map: @mapfileUrl
          FORMAT: 'image/png'
        }
        serverType: 'mapserver'
      })
    })

    osmLayer = new ol.layer.Tile({
      title: 'OSM'
      type: 'base'
      visible: true
      source: new ol.source.OSM()
    })

    satelliteLayer = new ol.layer.Tile({
      title: 'Satellite'
      type: 'base'
      visible: false
      source: new ol.source.MapQuest({layer: 'sat'})
    })

    @vectorLayer = VectorLayer.createLayer @renderedMap

    baseGroup = @createLayersGroup 'Base maps', [osmLayer, satelliteLayer]
    overlaysGroup = @createLayersGroup 'Overlays', [statesLayer, railwaysLayer,
      @vectorLayer]

    @renderedMap.addLayer baseGroup
    @renderedMap.addLayer overlaysGroup

  createLayersGroup: (groupTitle, groupLayers) ->
    return new ol.layer.Group({
      title: groupTitle
      layers: groupLayers
    })

  setControls: ->
    return ol.control.defaults().extend([
      new ol.control.ScaleLine({
        units: 'metric' #degrees
      }),
      new ol.control.ZoomSlider()
      #new ol.control.FullScreen(),
      #new ol.control.MousePosition()
    ])

  createLayerSwitcher: ->
    # tipLabel - Optional label for button
    layerSwitcher = new ol.control.LayerSwitcher({tipLabel: 'Legenda'})
    @renderedMap.addControl layerSwitcher

module?.exports = map
