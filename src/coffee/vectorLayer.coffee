###*
   * Objekt pro vytvareni vektorove vrstvy
###
vectorLayer =
  ###*
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
  ###
  layer: null

  ###*
     * Vykreslena mapa ol3
     * @type {Object}
  ###
  renderedMap: null

  ###*
     * Pole prvku
     * @type {Object}
  ###
  features: new ol.Collection()

  createLayer: (@renderedMap) ->
    @layer = new ol.layer.Vector({
      title: 'Vector'
      visible: false
      #source: vectorLayerSource,
      source: new ol.source.Vector({features: @features})
      ###
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
          color: '#ffcc33',
          width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
        })
      ###
    })

    @createTestingVectorData()
    @layerTools()

    return @layer

  createTestingVectorData: ->
    # Vytvoreni testovaciho polygonu
    thing = new ol.geom.Polygon( [[
      ol.proj.transform([15.2, 50], 'EPSG:4326', 'EPSG:3857')
      ol.proj.transform([15.5,50], 'EPSG:4326', 'EPSG:3857')
      ol.proj.transform([15.7,49.30], 'EPSG:4326', 'EPSG:3857')
      ol.proj.transform([15.0,49.30], 'EPSG:4326', 'EPSG:3857')
    ]])

    featurething = new ol.Feature({
      name: "Test polygon"
      geometry: thing
    })

    # Vytvoreni testovaciho bodu
    point = new ol.geom.Point(
      ol.proj.transform([15.35, 50.1], 'EPSG:4326', 'EPSG:3857')
    )

    featurePoint = new ol.Feature({
      name: "Test point"
      geometry: point
    })

    #vectorLayerSource.addFeature(featurething);
    @features.push featurething
    @features.push featurePoint

  layerTools: ->
    modify = new ol.interaction.Modify({
      features: @features
      #the SHIFT key must be pressed to delete vertices, so
      # that new vertices can be drawn at the same position
      # of existing vertices
      deleteCondition: (event) ->
        return ol.events.condition.shiftKeyOnly(event) &&
          ol.events.condition.singleClick(event)
    })

    @renderedMap.addInteraction modify
    # vypnout nastroje, pokud neni zobrazena vektorova vrstva

    modify.on 'modifyend', (e) ->
      newCoords = e.currentTarget.vertexFeature_.getGeometry().getCoordinates()
      console.log ol.proj.transform(newCoords, 'EPSG:3857', 'EPSG:4326')

    selectSingleClick = new ol.interaction.Select()
    @renderedMap.addInteraction selectSingleClick

    selectSingleClick.on 'select', (e) ->
      document.getElementById('status').innerHTML = "&nbsp; #{e.target.getFeatures().getLength()}
      selected features (last operation selected #{e.selected.length}
      and deselected #{e.deselected.length} features)"

module?.exports = vectorLayer
