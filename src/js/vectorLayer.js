
/**
   * Objekt pro vytvareni vektorove vrstvy
 */
var vectorLayer;

vectorLayer = {

  /**
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
   */
  layer: null,

  /**
     * Vykreslena mapa ol3
     * @type {Object}
   */
  renderedMap: null,

  /**
     * Pole prvku
     * @type {Object}
   */
  features: new ol.Collection(),
  createLayer: function(renderedMap) {
    this.renderedMap = renderedMap;
    this.layer = new ol.layer.Vector({
      title: 'Vector',
      visible: false,
      source: new ol.source.Vector({
        features: this.features
      })

      /*
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
          color: '#ffcc33',
          width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
        })
       */
    });
    this.createTestingVectorData();
    this.layerTools();
    return this.layer;
  },
  createTestingVectorData: function() {
    var featurePoint, featurething, point, thing;
    thing = new ol.geom.Polygon([[ol.proj.transform([15.2, 50], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.5, 50], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.7, 49.30], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.0, 49.30], 'EPSG:4326', 'EPSG:3857')]]);
    featurething = new ol.Feature({
      name: "Test polygon",
      geometry: thing
    });
    point = new ol.geom.Point(ol.proj.transform([15.35, 50.1], 'EPSG:4326', 'EPSG:3857'));
    featurePoint = new ol.Feature({
      name: "Test point",
      geometry: point
    });
    this.features.push(featurething);
    return this.features.push(featurePoint);
  },
  layerTools: function() {
    var modify, selectSingleClick;
    modify = new ol.interaction.Modify({
      features: this.features,
      deleteCondition: function(event) {
        return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
      }
    });
    this.renderedMap.addInteraction(modify);
    modify.on('modifyend', function(e) {
      var newCoords;
      newCoords = e.currentTarget.vertexFeature_.getGeometry().getCoordinates();
      return console.log(ol.proj.transform(newCoords, 'EPSG:3857', 'EPSG:4326'));
    });
    selectSingleClick = new ol.interaction.Select();
    this.renderedMap.addInteraction(selectSingleClick);
    return selectSingleClick.on('select', function(e) {
      return document.getElementById('status').innerHTML = "&nbsp; " + (e.target.getFeatures().getLength()) + " selected features (last operation selected " + e.selected.length + " and deselected " + e.deselected.length + " features)";
    });
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = vectorLayer;
}
