var VectorLayer, map;

VectorLayer = require('./vectorLayer');


/**
   * Objekt pro vytvareni mapy
 */

map = {

  /**
     * Vykreslena mapa ol3
     * @type {Object}
   */
  renderedMap: null,

  /**
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
   */
  vectorLayer: null,

  /**
     * Url pro zavolani mapserver
     * @type {String}
   */
  mapserverUrl: 'http://localhost/cgi-bin/mapserv',

  /**
     * Cesta k mapfile
     * @type {String}
   */
  mapfileUrl: '/srv/www/htdocs/ol3-mapserver-test/src/data/mapfile.map',

  /*
    * Inicializace OL3 mapy
    * @param {String} target - id elementu pro mapu
   */
  init: function(target) {
    this.renderedMap = new ol.Map({
      controls: this.setControls(),
      layers: [],
      view: this.setMapView(),
      target: target
    });
    this.setLayers();
    return this.createLayerSwitcher();
  },
  setMapView: function() {
    return new ol.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857'),
      zoom: 5,
      projection: 'EPSG:3857'
    });
  },
  setLayers: function() {
    var baseGroup, osmLayer, overlaysGroup, satelliteLayer, statesLayer;
    statesLayer = new ol.layer.Image({
      title: 'Countries',
      visible: false,
      source: new ol.source.ImageWMS({
        url: this.mapserverUrl,
        crossOrigin: 'anonymous',
        params: {
          LAYERS: 'world-political-line',
          map: this.mapfileUrl,
          FORMAT: 'image/png'
        },
        serverType: 'mapserver'
      })
    });
    osmLayer = new ol.layer.Tile({
      title: 'OSM',
      type: 'base',
      visible: true,
      source: new ol.source.OSM()
    });
    satelliteLayer = new ol.layer.Tile({
      title: 'Satellite',
      type: 'base',
      visible: false,
      source: new ol.source.MapQuest({
        layer: 'sat'
      })
    });
    this.vectorLayer = VectorLayer.createLayer(this.renderedMap);
    baseGroup = this.createLayersGroup('Base maps', [osmLayer, satelliteLayer]);
    overlaysGroup = this.createLayersGroup('Overlays', [statesLayer, railwaysLayer, this.vectorLayer]);
    this.renderedMap.addLayer(baseGroup);
    return this.renderedMap.addLayer(overlaysGroup);
  },
  createLayersGroup: function(groupTitle, groupLayers) {
    return new ol.layer.Group({
      title: groupTitle,
      layers: groupLayers
    });
  },
  setControls: function() {
    return ol.control.defaults().extend([
      new ol.control.ScaleLine({
        units: 'metric'
      }), new ol.control.ZoomSlider()
    ]);
  },
  createLayerSwitcher: function() {
    var layerSwitcher;
    layerSwitcher = new ol.control.LayerSwitcher({
      tipLabel: 'Legenda'
    });
    return this.renderedMap.addControl(layerSwitcher);
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = map;
}
