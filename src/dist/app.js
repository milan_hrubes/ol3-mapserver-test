(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.MapApp = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var VectorLayer, map;

VectorLayer = require('./vectorLayer');


/**
   * Objekt pro vytvareni mapy
 */

map = {

  /**
     * Vykreslena mapa ol3
     * @type {Object}
   */
  renderedMap: null,

  /**
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
   */
  vectorLayer: null,

  /**
     * Url pro zavolani mapserver
     * @type {String}
   */
  mapserverUrl: 'http://localhost/cgi-bin/mapserv',

  /**
     * Cesta k mapfile
     * @type {String}
   */
  mapfileUrl: '/srv/www/htdocs/ol3-mapserver-test/src/data/mapfile.map',

  /*
    * Inicializace OL3 mapy
    * @param {String} target - id elementu pro mapu
   */
  init: function(target) {
    this.renderedMap = new ol.Map({
      controls: this.setControls(),
      layers: [],
      view: this.setMapView(),
      target: target
    });
    this.setLayers();
    return this.createLayerSwitcher();
  },
  setMapView: function() {
    return new ol.View({
      center: ol.proj.transform([15, 50], 'EPSG:4326', 'EPSG:3857'),
      zoom: 5,
      projection: 'EPSG:3857'
    });
  },
  setLayers: function() {
    var baseGroup, osmLayer, overlaysGroup, satelliteLayer, statesLayer;
    statesLayer = new ol.layer.Image({
      title: 'Countries',
      visible: false,
      source: new ol.source.ImageWMS({
        url: this.mapserverUrl,
        crossOrigin: 'anonymous',
        params: {
          LAYERS: 'world-political-line',
          map: this.mapfileUrl,
          FORMAT: 'image/png'
        },
        serverType: 'mapserver'
      })
    });
    osmLayer = new ol.layer.Tile({
      title: 'OSM',
      type: 'base',
      visible: true,
      source: new ol.source.OSM()
    });
    satelliteLayer = new ol.layer.Tile({
      title: 'Satellite',
      type: 'base',
      visible: false,
      source: new ol.source.MapQuest({
        layer: 'sat'
      })
    });
    this.vectorLayer = VectorLayer.createLayer(this.renderedMap);
    baseGroup = this.createLayersGroup('Base maps', [osmLayer, satelliteLayer]);
    overlaysGroup = this.createLayersGroup('Overlays', [statesLayer, railwaysLayer, this.vectorLayer]);
    this.renderedMap.addLayer(baseGroup);
    return this.renderedMap.addLayer(overlaysGroup);
  },
  createLayersGroup: function(groupTitle, groupLayers) {
    return new ol.layer.Group({
      title: groupTitle,
      layers: groupLayers
    });
  },
  setControls: function() {
    return ol.control.defaults().extend([
      new ol.control.ScaleLine({
        units: 'metric'
      }), new ol.control.ZoomSlider()
    ]);
  },
  createLayerSwitcher: function() {
    var layerSwitcher;
    layerSwitcher = new ol.control.LayerSwitcher({
      tipLabel: 'Legenda'
    });
    return this.renderedMap.addControl(layerSwitcher);
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = map;
}

},{"./vectorLayer":2}],2:[function(require,module,exports){

/**
   * Objekt pro vytvareni vektorove vrstvy
 */
var vectorLayer;

vectorLayer = {

  /**
     * Vykreslena vrstva pro vektorova data
     * @type {Object}
   */
  layer: null,

  /**
     * Vykreslena mapa ol3
     * @type {Object}
   */
  renderedMap: null,

  /**
     * Pole prvku
     * @type {Object}
   */
  features: new ol.Collection(),
  createLayer: function(renderedMap) {
    this.renderedMap = renderedMap;
    this.layer = new ol.layer.Vector({
      title: 'Vector',
      visible: false,
      source: new ol.source.Vector({
        features: this.features
      })

      /*
        style: new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
          color: '#ffcc33',
          width: 2
          }),
          image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
              color: '#ffcc33'
            })
          })
        })
       */
    });
    this.createTestingVectorData();
    this.layerTools();
    return this.layer;
  },
  createTestingVectorData: function() {
    var featurePoint, featurething, point, thing;
    thing = new ol.geom.Polygon([[ol.proj.transform([15.2, 50], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.5, 50], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.7, 49.30], 'EPSG:4326', 'EPSG:3857'), ol.proj.transform([15.0, 49.30], 'EPSG:4326', 'EPSG:3857')]]);
    featurething = new ol.Feature({
      name: "Test polygon",
      geometry: thing
    });
    point = new ol.geom.Point(ol.proj.transform([15.35, 50.1], 'EPSG:4326', 'EPSG:3857'));
    featurePoint = new ol.Feature({
      name: "Test point",
      geometry: point
    });
    this.features.push(featurething);
    return this.features.push(featurePoint);
  },
  layerTools: function() {
    var modify, selectSingleClick;
    modify = new ol.interaction.Modify({
      features: this.features,
      deleteCondition: function(event) {
        return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
      }
    });
    this.renderedMap.addInteraction(modify);
    modify.on('modifyend', function(e) {
      var newCoords;
      newCoords = e.currentTarget.vertexFeature_.getGeometry().getCoordinates();
      return console.log(ol.proj.transform(newCoords, 'EPSG:3857', 'EPSG:4326'));
    });
    selectSingleClick = new ol.interaction.Select();
    this.renderedMap.addInteraction(selectSingleClick);
    return selectSingleClick.on('select', function(e) {
      return document.getElementById('status').innerHTML = "&nbsp; " + (e.target.getFeatures().getLength()) + " selected features (last operation selected " + e.selected.length + " and deselected " + e.deselected.length + " features)";
    });
  }
};

if (typeof module !== "undefined" && module !== null) {
  module.exports = vectorLayer;
}

},{}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYXBwLmpzIiwic3JjL2pzL3ZlY3RvckxheWVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInZhciBWZWN0b3JMYXllciwgbWFwO1xuXG5WZWN0b3JMYXllciA9IHJlcXVpcmUoJy4vdmVjdG9yTGF5ZXInKTtcblxuXG4vKipcbiAgICogT2JqZWt0IHBybyB2eXR2YXJlbmkgbWFweVxuICovXG5cbm1hcCA9IHtcblxuICAvKipcbiAgICAgKiBWeWtyZXNsZW5hIG1hcGEgb2wzXG4gICAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHJlbmRlcmVkTWFwOiBudWxsLFxuXG4gIC8qKlxuICAgICAqIFZ5a3Jlc2xlbmEgdnJzdHZhIHBybyB2ZWt0b3JvdmEgZGF0YVxuICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICB2ZWN0b3JMYXllcjogbnVsbCxcblxuICAvKipcbiAgICAgKiBVcmwgcHJvIHphdm9sYW5pIG1hcHNlcnZlclxuICAgICAqIEB0eXBlIHtTdHJpbmd9XG4gICAqL1xuICBtYXBzZXJ2ZXJVcmw6ICdodHRwOi8vbG9jYWxob3N0L2NnaS1iaW4vbWFwc2VydicsXG5cbiAgLyoqXG4gICAgICogQ2VzdGEgayBtYXBmaWxlXG4gICAgICogQHR5cGUge1N0cmluZ31cbiAgICovXG4gIG1hcGZpbGVVcmw6ICcvc3J2L3d3dy9odGRvY3Mvb2wzLW1hcHNlcnZlci10ZXN0L3NyYy9kYXRhL21hcGZpbGUubWFwJyxcblxuICAvKlxuICAgICogSW5pY2lhbGl6YWNlIE9MMyBtYXB5XG4gICAgKiBAcGFyYW0ge1N0cmluZ30gdGFyZ2V0IC0gaWQgZWxlbWVudHUgcHJvIG1hcHVcbiAgICovXG4gIGluaXQ6IGZ1bmN0aW9uKHRhcmdldCkge1xuICAgIHRoaXMucmVuZGVyZWRNYXAgPSBuZXcgb2wuTWFwKHtcbiAgICAgIGNvbnRyb2xzOiB0aGlzLnNldENvbnRyb2xzKCksXG4gICAgICBsYXllcnM6IFtdLFxuICAgICAgdmlldzogdGhpcy5zZXRNYXBWaWV3KCksXG4gICAgICB0YXJnZXQ6IHRhcmdldFxuICAgIH0pO1xuICAgIHRoaXMuc2V0TGF5ZXJzKCk7XG4gICAgcmV0dXJuIHRoaXMuY3JlYXRlTGF5ZXJTd2l0Y2hlcigpO1xuICB9LFxuICBzZXRNYXBWaWV3OiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gbmV3IG9sLlZpZXcoe1xuICAgICAgY2VudGVyOiBvbC5wcm9qLnRyYW5zZm9ybShbMTUsIDUwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKSxcbiAgICAgIHpvb206IDUsXG4gICAgICBwcm9qZWN0aW9uOiAnRVBTRzozODU3J1xuICAgIH0pO1xuICB9LFxuICBzZXRMYXllcnM6IGZ1bmN0aW9uKCkge1xuICAgIHZhciBiYXNlR3JvdXAsIG9zbUxheWVyLCBvdmVybGF5c0dyb3VwLCBzYXRlbGxpdGVMYXllciwgc3RhdGVzTGF5ZXI7XG4gICAgc3RhdGVzTGF5ZXIgPSBuZXcgb2wubGF5ZXIuSW1hZ2Uoe1xuICAgICAgdGl0bGU6ICdDb3VudHJpZXMnLFxuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBzb3VyY2U6IG5ldyBvbC5zb3VyY2UuSW1hZ2VXTVMoe1xuICAgICAgICB1cmw6IHRoaXMubWFwc2VydmVyVXJsLFxuICAgICAgICBjcm9zc09yaWdpbjogJ2Fub255bW91cycsXG4gICAgICAgIHBhcmFtczoge1xuICAgICAgICAgIExBWUVSUzogJ3dvcmxkLXBvbGl0aWNhbC1saW5lJyxcbiAgICAgICAgICBtYXA6IHRoaXMubWFwZmlsZVVybCxcbiAgICAgICAgICBGT1JNQVQ6ICdpbWFnZS9wbmcnXG4gICAgICAgIH0sXG4gICAgICAgIHNlcnZlclR5cGU6ICdtYXBzZXJ2ZXInXG4gICAgICB9KVxuICAgIH0pO1xuICAgIG9zbUxheWVyID0gbmV3IG9sLmxheWVyLlRpbGUoe1xuICAgICAgdGl0bGU6ICdPU00nLFxuICAgICAgdHlwZTogJ2Jhc2UnLFxuICAgICAgdmlzaWJsZTogdHJ1ZSxcbiAgICAgIHNvdXJjZTogbmV3IG9sLnNvdXJjZS5PU00oKVxuICAgIH0pO1xuICAgIHNhdGVsbGl0ZUxheWVyID0gbmV3IG9sLmxheWVyLlRpbGUoe1xuICAgICAgdGl0bGU6ICdTYXRlbGxpdGUnLFxuICAgICAgdHlwZTogJ2Jhc2UnLFxuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBzb3VyY2U6IG5ldyBvbC5zb3VyY2UuTWFwUXVlc3Qoe1xuICAgICAgICBsYXllcjogJ3NhdCdcbiAgICAgIH0pXG4gICAgfSk7XG4gICAgdGhpcy52ZWN0b3JMYXllciA9IFZlY3RvckxheWVyLmNyZWF0ZUxheWVyKHRoaXMucmVuZGVyZWRNYXApO1xuICAgIGJhc2VHcm91cCA9IHRoaXMuY3JlYXRlTGF5ZXJzR3JvdXAoJ0Jhc2UgbWFwcycsIFtvc21MYXllciwgc2F0ZWxsaXRlTGF5ZXJdKTtcbiAgICBvdmVybGF5c0dyb3VwID0gdGhpcy5jcmVhdGVMYXllcnNHcm91cCgnT3ZlcmxheXMnLCBbc3RhdGVzTGF5ZXIsIHJhaWx3YXlzTGF5ZXIsIHRoaXMudmVjdG9yTGF5ZXJdKTtcbiAgICB0aGlzLnJlbmRlcmVkTWFwLmFkZExheWVyKGJhc2VHcm91cCk7XG4gICAgcmV0dXJuIHRoaXMucmVuZGVyZWRNYXAuYWRkTGF5ZXIob3ZlcmxheXNHcm91cCk7XG4gIH0sXG4gIGNyZWF0ZUxheWVyc0dyb3VwOiBmdW5jdGlvbihncm91cFRpdGxlLCBncm91cExheWVycykge1xuICAgIHJldHVybiBuZXcgb2wubGF5ZXIuR3JvdXAoe1xuICAgICAgdGl0bGU6IGdyb3VwVGl0bGUsXG4gICAgICBsYXllcnM6IGdyb3VwTGF5ZXJzXG4gICAgfSk7XG4gIH0sXG4gIHNldENvbnRyb2xzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gb2wuY29udHJvbC5kZWZhdWx0cygpLmV4dGVuZChbXG4gICAgICBuZXcgb2wuY29udHJvbC5TY2FsZUxpbmUoe1xuICAgICAgICB1bml0czogJ21ldHJpYydcbiAgICAgIH0pLCBuZXcgb2wuY29udHJvbC5ab29tU2xpZGVyKClcbiAgICBdKTtcbiAgfSxcbiAgY3JlYXRlTGF5ZXJTd2l0Y2hlcjogZnVuY3Rpb24oKSB7XG4gICAgdmFyIGxheWVyU3dpdGNoZXI7XG4gICAgbGF5ZXJTd2l0Y2hlciA9IG5ldyBvbC5jb250cm9sLkxheWVyU3dpdGNoZXIoe1xuICAgICAgdGlwTGFiZWw6ICdMZWdlbmRhJ1xuICAgIH0pO1xuICAgIHJldHVybiB0aGlzLnJlbmRlcmVkTWFwLmFkZENvbnRyb2wobGF5ZXJTd2l0Y2hlcik7XG4gIH1cbn07XG5cbmlmICh0eXBlb2YgbW9kdWxlICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZSAhPT0gbnVsbCkge1xuICBtb2R1bGUuZXhwb3J0cyA9IG1hcDtcbn1cbiIsIlxuLyoqXG4gICAqIE9iamVrdCBwcm8gdnl0dmFyZW5pIHZla3Rvcm92ZSB2cnN0dnlcbiAqL1xudmFyIHZlY3RvckxheWVyO1xuXG52ZWN0b3JMYXllciA9IHtcblxuICAvKipcbiAgICAgKiBWeWtyZXNsZW5hIHZyc3R2YSBwcm8gdmVrdG9yb3ZhIGRhdGFcbiAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKi9cbiAgbGF5ZXI6IG51bGwsXG5cbiAgLyoqXG4gICAgICogVnlrcmVzbGVuYSBtYXBhIG9sM1xuICAgICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICByZW5kZXJlZE1hcDogbnVsbCxcblxuICAvKipcbiAgICAgKiBQb2xlIHBydmt1XG4gICAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIGZlYXR1cmVzOiBuZXcgb2wuQ29sbGVjdGlvbigpLFxuICBjcmVhdGVMYXllcjogZnVuY3Rpb24ocmVuZGVyZWRNYXApIHtcbiAgICB0aGlzLnJlbmRlcmVkTWFwID0gcmVuZGVyZWRNYXA7XG4gICAgdGhpcy5sYXllciA9IG5ldyBvbC5sYXllci5WZWN0b3Ioe1xuICAgICAgdGl0bGU6ICdWZWN0b3InLFxuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBzb3VyY2U6IG5ldyBvbC5zb3VyY2UuVmVjdG9yKHtcbiAgICAgICAgZmVhdHVyZXM6IHRoaXMuZmVhdHVyZXNcbiAgICAgIH0pXG5cbiAgICAgIC8qXG4gICAgICAgIHN0eWxlOiBuZXcgb2wuc3R5bGUuU3R5bGUoe1xuICAgICAgICAgIGZpbGw6IG5ldyBvbC5zdHlsZS5GaWxsKHtcbiAgICAgICAgICAgIGNvbG9yOiAncmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpJ1xuICAgICAgICAgIH0pLFxuICAgICAgICAgIHN0cm9rZTogbmV3IG9sLnN0eWxlLlN0cm9rZSh7XG4gICAgICAgICAgY29sb3I6ICcjZmZjYzMzJyxcbiAgICAgICAgICB3aWR0aDogMlxuICAgICAgICAgIH0pLFxuICAgICAgICAgIGltYWdlOiBuZXcgb2wuc3R5bGUuQ2lyY2xlKHtcbiAgICAgICAgICAgIHJhZGl1czogNyxcbiAgICAgICAgICAgIGZpbGw6IG5ldyBvbC5zdHlsZS5GaWxsKHtcbiAgICAgICAgICAgICAgY29sb3I6ICcjZmZjYzMzJ1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICB9KVxuICAgICAgICB9KVxuICAgICAgICovXG4gICAgfSk7XG4gICAgdGhpcy5jcmVhdGVUZXN0aW5nVmVjdG9yRGF0YSgpO1xuICAgIHRoaXMubGF5ZXJUb29scygpO1xuICAgIHJldHVybiB0aGlzLmxheWVyO1xuICB9LFxuICBjcmVhdGVUZXN0aW5nVmVjdG9yRGF0YTogZnVuY3Rpb24oKSB7XG4gICAgdmFyIGZlYXR1cmVQb2ludCwgZmVhdHVyZXRoaW5nLCBwb2ludCwgdGhpbmc7XG4gICAgdGhpbmcgPSBuZXcgb2wuZ2VvbS5Qb2x5Z29uKFtbb2wucHJvai50cmFuc2Zvcm0oWzE1LjIsIDUwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKSwgb2wucHJvai50cmFuc2Zvcm0oWzE1LjUsIDUwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKSwgb2wucHJvai50cmFuc2Zvcm0oWzE1LjcsIDQ5LjMwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKSwgb2wucHJvai50cmFuc2Zvcm0oWzE1LjAsIDQ5LjMwXSwgJ0VQU0c6NDMyNicsICdFUFNHOjM4NTcnKV1dKTtcbiAgICBmZWF0dXJldGhpbmcgPSBuZXcgb2wuRmVhdHVyZSh7XG4gICAgICBuYW1lOiBcIlRlc3QgcG9seWdvblwiLFxuICAgICAgZ2VvbWV0cnk6IHRoaW5nXG4gICAgfSk7XG4gICAgcG9pbnQgPSBuZXcgb2wuZ2VvbS5Qb2ludChvbC5wcm9qLnRyYW5zZm9ybShbMTUuMzUsIDUwLjFdLCAnRVBTRzo0MzI2JywgJ0VQU0c6Mzg1NycpKTtcbiAgICBmZWF0dXJlUG9pbnQgPSBuZXcgb2wuRmVhdHVyZSh7XG4gICAgICBuYW1lOiBcIlRlc3QgcG9pbnRcIixcbiAgICAgIGdlb21ldHJ5OiBwb2ludFxuICAgIH0pO1xuICAgIHRoaXMuZmVhdHVyZXMucHVzaChmZWF0dXJldGhpbmcpO1xuICAgIHJldHVybiB0aGlzLmZlYXR1cmVzLnB1c2goZmVhdHVyZVBvaW50KTtcbiAgfSxcbiAgbGF5ZXJUb29sczogZnVuY3Rpb24oKSB7XG4gICAgdmFyIG1vZGlmeSwgc2VsZWN0U2luZ2xlQ2xpY2s7XG4gICAgbW9kaWZ5ID0gbmV3IG9sLmludGVyYWN0aW9uLk1vZGlmeSh7XG4gICAgICBmZWF0dXJlczogdGhpcy5mZWF0dXJlcyxcbiAgICAgIGRlbGV0ZUNvbmRpdGlvbjogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgcmV0dXJuIG9sLmV2ZW50cy5jb25kaXRpb24uc2hpZnRLZXlPbmx5KGV2ZW50KSAmJiBvbC5ldmVudHMuY29uZGl0aW9uLnNpbmdsZUNsaWNrKGV2ZW50KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnJlbmRlcmVkTWFwLmFkZEludGVyYWN0aW9uKG1vZGlmeSk7XG4gICAgbW9kaWZ5Lm9uKCdtb2RpZnllbmQnLCBmdW5jdGlvbihlKSB7XG4gICAgICB2YXIgbmV3Q29vcmRzO1xuICAgICAgbmV3Q29vcmRzID0gZS5jdXJyZW50VGFyZ2V0LnZlcnRleEZlYXR1cmVfLmdldEdlb21ldHJ5KCkuZ2V0Q29vcmRpbmF0ZXMoKTtcbiAgICAgIHJldHVybiBjb25zb2xlLmxvZyhvbC5wcm9qLnRyYW5zZm9ybShuZXdDb29yZHMsICdFUFNHOjM4NTcnLCAnRVBTRzo0MzI2JykpO1xuICAgIH0pO1xuICAgIHNlbGVjdFNpbmdsZUNsaWNrID0gbmV3IG9sLmludGVyYWN0aW9uLlNlbGVjdCgpO1xuICAgIHRoaXMucmVuZGVyZWRNYXAuYWRkSW50ZXJhY3Rpb24oc2VsZWN0U2luZ2xlQ2xpY2spO1xuICAgIHJldHVybiBzZWxlY3RTaW5nbGVDbGljay5vbignc2VsZWN0JywgZnVuY3Rpb24oZSkge1xuICAgICAgcmV0dXJuIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzdGF0dXMnKS5pbm5lckhUTUwgPSBcIiZuYnNwOyBcIiArIChlLnRhcmdldC5nZXRGZWF0dXJlcygpLmdldExlbmd0aCgpKSArIFwiIHNlbGVjdGVkIGZlYXR1cmVzIChsYXN0IG9wZXJhdGlvbiBzZWxlY3RlZCBcIiArIGUuc2VsZWN0ZWQubGVuZ3RoICsgXCIgYW5kIGRlc2VsZWN0ZWQgXCIgKyBlLmRlc2VsZWN0ZWQubGVuZ3RoICsgXCIgZmVhdHVyZXMpXCI7XG4gICAgfSk7XG4gIH1cbn07XG5cbmlmICh0eXBlb2YgbW9kdWxlICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZSAhPT0gbnVsbCkge1xuICBtb2R1bGUuZXhwb3J0cyA9IHZlY3RvckxheWVyO1xufVxuIl19
