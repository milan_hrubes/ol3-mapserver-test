gulp = require 'gulp'
gutil = require 'gulp-util'
nib = require 'nib'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'
concat = require 'gulp-concat'
browserify = require 'browserify'
through2 = require 'through2'
yargs = require('yargs').argv
rename = require 'gulp-rename'
#transform = require 'vinyl-transform'
#vss = require 'vinyl-source-stream'

handleError = (e, cb) ->
  gutil.log gutil.colors.red('Error'), e
  @emit 'end'
  #cb?()

paths =
  coffee:
    src: './src/coffee/**/*.coffee'
    dist: './src/js'
  js:
    src: ['./src/js/**/*.js']
    dest: './src/js/app.js'
  dist: './src/dist'

gulp.task 'browserify', ->
  options =
    insertGlobals : false
    debug: !yargs.production
    standalone: 'MapApp'

  gulp.src(paths.js.dest)
    .pipe through2.obj (file, enc, next) ->
      browserify(file.path, options)
        .transform 'browserify-shim'
        .bundle (err, res) ->
          #assumes file.contents is a Buffer
          file.contents = res
          next null, file

    .pipe(gulp.dest(paths.dist))

# zatim upraveno pro example
gulp.task 'coffee', ->
  gulp.src(paths.coffee.src)
    .pipe(coffee({bare: true}).on('error', handleError))
    .pipe(gulp.dest(paths.coffee.dist))

# zatim upraveno pro example
gulp.task 'compileJs', ['coffee'], ->
  gulp.src('./src/dist/app.js')
    #.pipe(concat('app.min.js'))
    .pipe(uglify().on('error', handleError))
    .pipe(rename({extname: '.min.js'}))
    .pipe(gulp.dest(paths.dist))

gulp.task 'watch', ->
  gulp.watch paths.coffee.src, ['coffee', 'compileJs']
  gulp.watch paths.styl.src, -> gulp.start 'stylus'

gulp.task 'default', ['coffee', 'browserify', 'compileJs'], ->
