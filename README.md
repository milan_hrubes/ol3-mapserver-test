# Ol3-mapserver-test

App for displaying maps with Mapserver.

## Getting Started

To use this app you must have installed [mapserver](http://mapserver.org/download.html)(>6.2)

* Download the latest release on [Bitbucket](https://bitbucket.org/milan_hrubes/ol3-mapserver-test)
* bower install
* npm install
* gulp

## Creator

This app was created by and is maintained by **Milan Hrubeš**.

* https://twitter.com/mildahrubes
* https://bitbucket.org/milan_hrubes/

## Copyright and License

Copyright 2015 Milan Hrubeš released under the [MIT](https://bitbucket.org/milan_hrubes/ol3-mapserver-test/src/LICENSE) license.
